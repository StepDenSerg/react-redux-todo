import {Action, Reducer} from "../../interfaces/redux-types";
import {UndoableActionTypes} from "./undoable.actions";
import produce from "immer";


interface UndoableState {
    past: any[],
    present: any,
    future: any[]
}

export function undoable(reducer: Reducer): Reducer {
    const initialState: UndoableState = {
        past: [],
        present: reducer(undefined, {type: 'undo-placeholder'}),
        future: []
    };

    return function (state = initialState, action: Action) {
        return produce(state, draft => {
            switch (action.type) {
                case UndoableActionTypes.undo:
                    draft.future = [draft.present, ...draft.future];
                    draft.present = draft.past[draft.past.length - 1];
                    draft.past = draft.past.slice(0, draft.past.length - 1);
                    break;
                case UndoableActionTypes.redo:
                    draft.past = [...draft.past, draft.present];
                    draft.present = draft.future[0];
                    draft.future = draft.future.slice(1);
                    break;
                default:
                    const newPresent = reducer(draft.present, action);

                    if (newPresent !== draft.present) {
                        draft.past = [...draft.past, draft.present];
                        draft.present = newPresent;
                        draft.future = [];
                    }
            }
        });
    }
}

import {Action} from "../../interfaces/redux-types";

export const UndoableActionTypes = {
    undo: 'UNDO',
    redo: 'REDO'
}

export const undo = (): Action => {
    return {type: UndoableActionTypes.undo};
};

export const redo = (): Action => {
    return {type: UndoableActionTypes.redo};
};

import {FiltersState} from "./filters.interfaces";
import {Action} from "../../interfaces/redux-types";
import produce from "immer";
import {FiltersActions} from "./filters.actions";


const initialFiltersState: FiltersState = {
    currentFilterIndex: 0
}

export const filters = produce((draft: FiltersState, action: Action) => {
    switch (action.type) {
        case FiltersActions.set:
            draft.currentFilterIndex = action.payload.index
    }
}, initialFiltersState)


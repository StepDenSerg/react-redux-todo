import {Action} from "../../interfaces/redux-types";

export const FiltersActions = {
    set: '[FILTERS] SET_FILTER'
}

export const setFilter = (index: number): Action => {
    return {type: FiltersActions.set, payload: {index}};
};


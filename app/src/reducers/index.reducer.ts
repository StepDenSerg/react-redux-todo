import {combineReducers} from 'redux'
import {undoable} from "../hor/undoable/undoable";
import {filters} from "./filters/filters.reducer";
import {tasks} from "./tasks/tasks.reducer";

export const rootReducer = combineReducers({
    filters,
    tasks: undoable(tasks)
});

import {Task} from "../../classes/tasks.classes";

export interface TasksState {
    tasksList: Task[]
}

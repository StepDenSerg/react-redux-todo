import {Action} from "../../interfaces/redux-types";

export const TasksActions = {
    add: '[TASKS] ADD_TASK',
    remove: '[TASKS] REMOVE_TASK',
    change: '[TASKS] CHANGE_TASK_STATE'

}

export const addTask = (text: string): Action => {
    return {type: TasksActions.add, payload: {text}};
};

export const removeTask = (id: number): Action => {
    return {type: TasksActions.remove, payload: {id}};
};

export const changeTaskState = (id: number): Action => {
    return {type: TasksActions.change, payload: {id}};
};

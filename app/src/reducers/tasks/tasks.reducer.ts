import {TasksState} from "./tasks.interfaces";
import {Action} from "../../interfaces/redux-types";
import {Task} from "../../classes/tasks.classes";
import produce from "immer";
import {TasksActions} from "./tasks.actions";

const initialTasksState: TasksState = {
    tasksList: [
        new Task('Task 1'),
        new Task('Task 2')
    ]
}

export const tasks = produce((draft, action: Action) => {
    switch (action.type) {
        case TasksActions.add:
            draft.tasksList.push(new Task(action.payload.text));
        case TasksActions.remove:
            draft.tasksList = draft.tasksList.filter((task: Task) => task.id !== action.payload.id);
        case TasksActions.change:
            const index = draft.tasksList.findIndex(task => task.id === action.payload.id);
            if (index >= 0) {
                const isCompleted = draft.tasksList[index].isCompleted;
                draft.tasksList[index] = {...draft.tasksList[index], isCompleted: !isCompleted}
            }
    }
}, initialTasksState);


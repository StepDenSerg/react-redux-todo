import React from 'react';
import classes from './tasks-output.container.module.scss';
import {connect} from 'react-redux';
import {TasksState} from "../../reducers/tasks/tasks.interfaces";
import {AppState} from "../../interfaces/redux-types";
import TaskComponent from "./task/task.component";
import {FiltersState} from "../../reducers/filters/filters.interfaces";
import {Task} from "../../classes/tasks.classes";
import {changeTaskState, removeTask} from "../../reducers/tasks/tasks.actions";

interface TasksOutputProps {
    tasks: TasksState,
    filters: FiltersState,
    removeTask: (n: number) => void,
    changeTaskState: (n: number) => void
}

interface TasksOutputState {
    visibleTasks: Task[],
}

class TasksOutputContainer extends React.PureComponent<TasksOutputProps> {
    state: TasksOutputState = {
        visibleTasks: []
    };

    constructor(props: TasksOutputProps) {
        super(props);
        this.state.visibleTasks = this.getVisibleTasks(
            this.props.tasks.tasksList,
            this.props.filters.currentFilterIndex
        );
    }

    componentWillReceiveProps(newProps: TasksOutputProps) {
        this.setState({
            visibleTasks: this.getVisibleTasks(
                newProps.tasks.tasksList,
                newProps.filters.currentFilterIndex
            )
        });
    }

    checkTask = (id: number) => {
        this.props.changeTaskState(id);
    };

    removeTask = (id: number) => {
        this.props.removeTask(id);
    };

    private getVisibleTasks(tasks: Task[], currentFilterIndex: number) {
        return tasks
            .filter(task => {
                if (currentFilterIndex === 1) {
                    return task.isCompleted;
                } else if (currentFilterIndex === 2) {
                    return !task.isCompleted;
                } else {
                    return true;
                }
            });
    }

    render() {
        const tasksTemplate = this.state.visibleTasks
            .map(task =>
                <TaskComponent
                    key={task.id}
                    task={task}
                    change={this.checkTask}
                    remove={this.removeTask}
                />
            );

        return (
            <div className={classes.TasksOutput}>
                <div>
                    {this.state.visibleTasks.length ?
                        tasksTemplate :
                        <span>List is Empty!</span>}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        filters: state.filters,
        tasks: state.tasks.present
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        removeTask: (id: number) => {
            dispatch(removeTask(id))
        },
        changeTaskState: (id: number) => {
            dispatch(changeTaskState(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TasksOutputContainer);

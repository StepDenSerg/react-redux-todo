export class Task {
    public id = Math.random() * 10;
    public isCompleted = false;

    constructor(
        public text: string,
    ) {}
}

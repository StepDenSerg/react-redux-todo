import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import * as serviceWorker from './serviceWorker';
import {composeWithDevTools} from "redux-devtools-extension";
import {rootReducer} from "./reducers/index.reducer";
import AppComponent from "./App.component";

const store = createStore(rootReducer, composeWithDevTools());

ReactDOM.render(<Provider store={store}><AppComponent /></Provider>, document.getElementById('root'));

serviceWorker.register();

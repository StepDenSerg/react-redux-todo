import {rootReducer} from "../reducers/index.reducer";

export interface Action {
    type: string,
    payload?: any
}

export type Reducer = (state: any, action: Action) => any;

export type AppState = ReturnType<typeof rootReducer>;

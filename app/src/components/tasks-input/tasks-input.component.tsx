import React, {useState} from 'react';
import {connect} from 'react-redux';
import {addTask} from "../../reducers/tasks/tasks.actions";
import classes from './tasks-input.component.module.scss';

interface TasksInputDispatchProps {
    addTask: typeof addTask,
}

const TasksInput = (props: TasksInputDispatchProps) => {
    const [inputValue, updateInputValue] = useState('');

    const addTask = () => {
        if (inputValue) {
            updateInputValue('');
            props.addTask(inputValue);
        }
    };

    return (
        <div className={classes.TasksInputContainer}>
            <form
                onSubmit={(e) => {e.preventDefault();addTask()}}
            >
                <input
                    placeholder={'Enter new Task'}
                    className={classes.InputField}
                    value={inputValue}
                    onChange={(e) => updateInputValue(e.target.value)}
                    type="text"
                />
            </form>
        </div>
    )
};

const mapDispatchToProps = (dispatch: any): TasksInputDispatchProps => {
    return {
        addTask: (text: string) => dispatch(addTask(text))
    }
};

export default connect(null, mapDispatchToProps)(TasksInput);

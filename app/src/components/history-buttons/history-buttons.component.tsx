import {connect} from 'react-redux';
import {AppState} from "../../interfaces/redux-types";
import {redo, undo} from "../../hor/undoable/undoable.actions";
import Button from "../button/button.component";
import React from "react";

interface HistoryButtonsProps {
    undo: () => void,
    redo: () => void,
    future: AppState[],
    past: AppState[]
}

/**
 * !!!
 * Question: Why rerendering of FiltersBar doesn't cause rerendering of this component?
 * !!!
 */

const HistoryButtons = (props: HistoryButtonsProps) => (
        <React.Fragment>
            <Button disabled={!props.past.length} clicked={props.undo}>
                <i className="fas fa-undo"></i>
            </Button>
            <Button disabled={!props.future.length} clicked={props.redo}>
                <i className="fas fa-redo"></i>
            </Button>
        </React.Fragment>
);

const mapStateToProps = (state: AppState) => {
    return {
        future: state.tasks.future,
        past: state.tasks.past,
    }
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        undo: () => {
            dispatch(undo())
        },
        redo: () => {
            dispatch(redo())
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HistoryButtons);

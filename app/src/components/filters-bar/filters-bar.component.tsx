import React from 'react';
import classes from "./filters-bar.component.module.scss";
import {FiltersState} from "../../reducers/filters/filters.interfaces";
import {AppState} from "../../interfaces/redux-types";
import HistoryButtons from "../history-buttons/history-buttons.component";
import Button from "../button/button.component";
import {setFilter} from "../../reducers/filters/filters.actions";
import {connect} from "react-redux";

interface FiltersBarProps {
    setFilter: (index: number) => void,
    filters: FiltersState,
}

const FiltersBar = (props: FiltersBarProps) => {
    return (
        <div className={classes.FiltersWrapper}>
            <div className={classes.FiltersBar}>
                <div>
                    <Button filter={true} active={props.filters.currentFilterIndex === 0}
                            clicked={() => props.setFilter(0)}>All</Button>
                    <Button filter={true} active={props.filters.currentFilterIndex === 1}
                            clicked={() => props.setFilter(1)}>Completed</Button>
                    <Button filter={true} active={props.filters.currentFilterIndex === 2}
                            clicked={() => props.setFilter(2)}>Active</Button>
                </div>
                <div>
                    <HistoryButtons />
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state: AppState) => {
    return {
        filters: state.filters,
    }
};


const mapDispatchToProps = (dispatch: any) => {
    return {
        setFilter: (index: number) => {
            dispatch(setFilter(index))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersBar);

import React from 'react';
import classes from './button.component.module.scss';

interface ButtonProps {
    children: any,
    clicked: () => void,
    style?: any,
    active?: boolean,
    filter?: boolean,
    disabled?: boolean,
    remove?: boolean
}

const Button = (props: ButtonProps) => {
    const classNames = [
        classes.Button,
        props.active ? classes.Active : '',
        props.filter ? classes.Filter : '',
        props.remove ? classes.Remove : '',
    ].join(' ');

    return (
        <button
            disabled={props.disabled}
            style={props.style}
            onClick={props.clicked}
            className={classNames}
        >{props.children}</button>
    )
};

export default React.memo(Button);

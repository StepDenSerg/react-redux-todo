import React from 'react';
import classes from './app-content.component.module.scss';
import TasksInput from '../tasks-input/tasks-input.component';
import TasksOutputContainer from "../../containers/tasks-output/tasks-output.container";
import FiltersBar from "../filters-bar/filters-bar.component";


export const AppContent = () => (
    <div className={classes.AppContent}>
        <TasksInput />
        <FiltersBar />
        <TasksOutputContainer />
    </div>
)

export default AppContent;
